<?php

/**
 * Распечатка массива
 **/
function print_arr($array){
    echo "<pre>" . print_r($array, true) . "</pre>";
}

/**
 * Получение массива категорий
 **/
function get_groups(){
    global $connection;


    $query = "SELECT * FROM groups";
    $res = mysqli_query($connection, $query);

    $arr_cat = array();

    while($row = mysqli_fetch_assoc($res)){

        $arr_cat[$row['id']] = $row;
    }

    return $arr_cat;
}



/**
 * Построение дерева
 **/
function map_tree($dataset) {
    $tree = array();

    foreach ($dataset as $id=>&$node) {
        if (!$node['id_parent']){
            $tree[$id] = &$node;
        }else{
            $dataset[$node['id_parent']]['childs'][$id] = &$node;
        }
    }

    return $tree;
}

/**
 * Дерево в строку HTML
 **/
function groups_to_html($data){
    foreach($data as $item){
        $string .= groups_to_template($item);
    }

    return $string;
}

/**
 * Шаблон вывода категорий
 **/
function groups_to_template($group){
    ob_start();
    include 'groups_template.php';
    return ob_get_clean();
}

/**
 * Хлебные крошки
 **/
function breadcrumbs($array, $id){
    if(!$id) return false;

    $count = count($array);
    $breadcrumbs_array = array();
    for($i = 0; $i < $count; $i++){
        if($array[$id]){
            $breadcrumbs_array[$array[$id]['id']] = $array[$id]['name'];
            $id = $array[$id]['id_parent'];
        }else break;
    }
    return array_reverse($breadcrumbs_array, true);
}

/**
 * Получение ID дочерних категорий
 **/
function cats_id($array, $id){
    if(!$id) return false;

    foreach($array as $item){
        if($item['id_parent'] == $id){
            $data .= $item['id'] . ",";
            $data .= cats_id($array, $item['id']);
        }
    }
    return $data;
}

/**
 * Получение товаров
 **/
function get_products($ids = false){
    global $connection;
    if($ids){
        $query = "SELECT * FROM products WHERE id_group IN($ids) ORDER BY name";
    }else{
        $query = "SELECT * FROM products ORDER BY name";
    }
    $res = mysqli_query($connection, $query);
    $products = array();
    while($row = mysqli_fetch_assoc($res)){
        $products[] = $row;
    }
    return $products;
}

