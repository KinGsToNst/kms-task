<?php
include 'config.php';
include 'functions.php';
$groups = get_groups();
$groups_tree = map_tree($groups);

//строим дерево html
$groups_menu = groups_to_html($groups_tree);

if(isset($_GET['group'])){

    $id = (int)$_GET['group'];
    // хлебные крошки
    // return true (array not empty) || return false
    $breadcrumbs_array = breadcrumbs($groups, $id);

    if($breadcrumbs_array){
        $breadcrumbs = "<a href='/'>Главная</a> / ";
        foreach($breadcrumbs_array as $id => $name){
            $breadcrumbs .= "<a href='?group={$id}'>{$name}</a> / ";
        }
        $breadcrumbs = rtrim($breadcrumbs, " / ");
        $breadcrumbs = preg_replace("#(.+)?<a.+>(.+)</a>$#", "$1$2", $breadcrumbs);
    }else{
        $breadcrumbs = "<a href='/'>Главная</a> / Каталог";
    }

    // ID дочерних категорий
    $ids = cats_id($groups, $id);
    $ids = !$ids ? $id : rtrim($ids, ",");



    if($ids) $products = get_products($ids);
    else $products = null;
}else{
    $products = get_products();
}


